import express from "express"

const app = express()
const port = 8080

app.get('/', (req, res) => {
    res.send('This is my Node Application')
})

app.listen(port, () => {
    console.log(`Application is listening at https://localhost:${port}`)
})